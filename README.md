# Plotting various types of variables using matlab

This project aims to provide a set of key functions useful for plotting and visualizing specific types of variables (e.g. time series, RDMs, 1-dim variables) using matlab. 

#### ```fct_plot_Xt(t,Xt,prm)``` - Plotting the distribution of a time series

The function [fct_plot_Xt.m](fct_plot_Xt.m) allows plotting the distribution of a time series, with CI and significant difference with the baseline, with a convenient set of specifying parameters.

DEPENDENCIES: 
 - [pbci.m](https://github.com/GRousselet/matlab_stats/blob/master/pbci.m) - a function computing a percentile bootstrap confidence interval, developed by Guillaume Rousselet - University of Glasgow.

![fct_plot_Xt function Output Example](demo/demo_fct_plot_Xt_ex1.png)
