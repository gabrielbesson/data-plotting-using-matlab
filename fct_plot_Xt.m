function Xt_p = fct_plot_Xt(t, Xt, prm)
% function Xt_p = fct_plot_Xt(t, Xt, prm)
% 
% Plot the distribution of a time series, with CI and significant difference with the baseline.
%   - t: vector of timepoints
%   - Xt: a X*t matrix, with X the number of observation (rows), and t the
%          number of timepoint
%
% Exemple of usage:
%     fct_plot_Xt(times, vals_Xt);
%     fct_plot_Xt(times, vals_tX, struct('Xdim', 2 ...
%                                       ,'CI_mth', '95p_m'...
%                                       ,'bsl_stat_mth', 'param'...
%                                       ,'bsl', 0 ...
%                                       ,'offset', offset...
%                                       ,'plot_col', cmap(R,:)...
%                                       ));
%     fct_plot_Xt(times, f_avg(vals_Xt,1), struct('Xdim', 1 ...
%                                                  ,'CI_data', fct_calc_CI_dim(vals_Xt,1)...
%                                                  ...,'bsl_stat_mth', 'param'...
%                                                  ,'bsl_stat_data', fct_calc_bsl_stat(vals_Xt) ...
%                                                  ,'bsl', 0 ...
%                                                  ,'offset', offset...
%                                                  ,'plot_col', cmap(R,:)...
%                                                  ));
%
% if nargin<2,    error('fct_plot_Xt(): two arg necessary.');    end
% if ~exist('prm', 'var'), prm = struct;  end
% % Main
% if ~isfield(prm, 'Xdim'),           prm.Xdim = 1;    end
% if ~isfield(prm, 'CI_plot'),        prm.CI_plot = true;   end
% if ~isfield(prm, 'bsl_plot'),       prm.bsl_plot = true;    end
% if ~isfield(prm, 'bsl_stat_plot'),  prm.bsl_stat_plot = true;  end
% if ~isfield(prm, 'trials_plot'),    prm.trials_plot = false;  end
% if ~isfield(prm, 'f_avg'),          prm.f_avg = @nanmean;   end
% if ~isfield(prm, 'CI_mth'),         prm.CI_mth = 'ste';   end   % 'ste'  '95p_m'  '95p_med'
% if ~isfield(prm, 'CI_data'),        prm.CI_data = [];   end
% if ~isfield(prm, 'bsl'),            prm.bsl = 0;            end
% if ~isfield(prm, 'bsl_stat_mth'),   prm.bsl_stat_mth = 'param';  end % 'param' 'non-param'
% if ~isfield(prm, 'bsl_stat_data'),  prm.bsl_stat_data = [];  end
% if ~isfield(prm, 'bsl_sig_alpha'),  prm.bsl_sig_alpha = .05;  end
% if ~isfield(prm, 'bsl_sig_offset'), prm.bsl_sig_offset = 0;  end
% if ~isfield(prm, 'offset'),         prm.offset = 0;            end
% if ~isfield(prm, 'plot_col'),       prm.plot_col = [0 94 176]/255;  end
% % Others
% if ~isfield(prm, 'plot_fillalpha'), prm.plot_fillalpha = .2;  end
% if ~isfield(prm, 'plot_linewidth'), prm.plot_linewidth = 2;  end
% if ~isfield(prm, 'plot_linetype'),  prm.plot_linetype = '-';  end
% if ~isfield(prm, 'plot_linealpha'), prm.plot_linealpha = 1;  end
% if ~isfield(prm, 'plot_marksiz'),   prm.plot_marksiz = 6;  end
% if ~isfield(prm, 'plot_mth'),       prm.plot_mth = 'fill';  end  % 'fill'  'err_bar'
% if ~isfield(prm, 'CI_mth_Nb'),      prm.CI_mth_Nb = 100; end
% if ~isfield(prm, 'CI_mth_alpha'),   prm.CI_mth_alpha = .05; end
% if ~isfield(prm, 'bsl_plot_col'),   prm.bsl_plot_col = [1 1 1]*.7;    end
% if ~isfield(prm, 'bsl_plot_linetype'), prm.bsl_plot_linetype = ':';    end
% if ~isfield(prm, 'bsl_plot_linewidth'), prm.bsl_plot_linewidth = 2;    end
% if ~isfield(prm, 'bsl_sig_col'),    prm.bsl_sig_col = prm.plot_col;  end
% if ~isfield(prm, 'bsl_sig_linew'),  prm.bsl_sig_linew = 5;  end
% if ~isfield(prm, 'trials_col'),     prm.trials_col = [1 1 1]*.7;  end
% if ~isfield(prm, 'trials_alpha'),   prm.trials_alpha = .17;  end
%
% DEPENDENCIES: 
%   - pbci.m - a function computing a percentile bootstrap confidence interval.
%              Developed by Guillaume Rousselet - University of Glasgow.
%              Available here: https://github.com/GRousselet/matlab_stats.
%
%
% Gabriel Besson, 2021, University of Coimbra.
%
% 
% VERSION UPDATES
%   - 2021-05-21.   Initial development.
%   - 2023-09-13.   Adding 'Xt_p' as the output.



%% Parameters (input vars)
if nargin<2,    error('fct_plot_Xt(): two arg necessary.');    end
if ~exist('prm', 'var'), prm = struct;  end
% Main
if ~isfield(prm, 'Xdim'),           prm.Xdim = 1;    end
if ~isfield(prm, 'CI_plot'),        prm.CI_plot = true;   end
if ~isfield(prm, 'bsl_plot'),       prm.bsl_plot = true;    end
if ~isfield(prm, 'bsl_stat_plot'),  prm.bsl_stat_plot = true;  end
if ~isfield(prm, 'trials_plot'),    prm.trials_plot = false;  end
if ~isfield(prm, 'f_avg'),          prm.f_avg = @nanmean;   end
if ~isfield(prm, 'CI_mth'),         prm.CI_mth = 'ste';   end   % 'ste'  '95p_m'  '95p_med'
if ~isfield(prm, 'CI_data'),        prm.CI_data = [];   end
if ~isfield(prm, 'bsl'),            prm.bsl = 0;            end
if ~isfield(prm, 'bsl_stat_mth'),   prm.bsl_stat_mth = 'param';  end % 'param' 'non-param'
if ~isfield(prm, 'bsl_stat_data'),  prm.bsl_stat_data = [];  end
if ~isfield(prm, 'bsl_sig_alpha'),  prm.bsl_sig_alpha = .05;  end
if ~isfield(prm, 'bsl_sig_offset'), prm.bsl_sig_offset = 0;  end
if ~isfield(prm, 'offset'),         prm.offset = 0;            end
if ~isfield(prm, 'plot_col'),       prm.plot_col = [0 94 176]/255;  end
% Others
if ~isfield(prm, 'plot_fillalpha'), prm.plot_fillalpha = .2;  end
if ~isfield(prm, 'plot_linewidth'), prm.plot_linewidth = 2;  end
if ~isfield(prm, 'plot_linetype'),  prm.plot_linetype = '-';  end
if ~isfield(prm, 'plot_linealpha'), prm.plot_linealpha = 1;  end
if ~isfield(prm, 'plot_marksiz'),   prm.plot_marksiz = 6;  end
if ~isfield(prm, 'plot_mth'),       prm.plot_mth = 'fill';  end  % 'fill'  'err_bar'
if ~isfield(prm, 'CI_mth_Nb'),      prm.CI_mth_Nb = 100; end
if ~isfield(prm, 'CI_mth_alpha'),   prm.CI_mth_alpha = .05; end
if ~isfield(prm, 'bsl_plot_col'),   prm.bsl_plot_col = [1 1 1]*.7;    end
if ~isfield(prm, 'bsl_plot_linetype'), prm.bsl_plot_linetype = ':';    end
if ~isfield(prm, 'bsl_plot_linewidth'), prm.bsl_plot_linewidth = 2;    end
if ~isfield(prm, 'bsl_sig_col'),    prm.bsl_sig_col = prm.plot_col;  end
if ~isfield(prm, 'bsl_sig_linew'),  prm.bsl_sig_linew = 5;  end
if ~isfield(prm, 'trials_col'),     prm.trials_col = [1 1 1]*.7;  end
if ~isfield(prm, 'trials_alpha'),   prm.trials_alpha = .17;  end


%% Format check
% 't'
if ~(ndims(t)==2 && any(size(t)==1)), error('''%s'' is not a column or row vector, as required for the t values of a Xt plot.', inputname(1));   end

% 'Xt'
Xt = fct_squeeze_and_bring_dim_first(Xt,prm.Xdim); % Placing dim of X in the first dimension while squeezing other singleton dimesions
if ~(ndims(Xt)==2 && size(Xt,2)==numel(t)), error('''Xt'' is not shaped for ''t'', for a Xt plot. Try aiming the good dim with the parameter ''Xdim''.');   end


%% Computations

% Def: 'Xt_avg'
if size(Xt,1)==1
    Xt_avg = Xt;
else
    Xt_avg = prm.f_avg(Xt,1);
end
    
    
% Def: 'Xt_CI'
if ~prm.CI_plot || (isempty(prm.CI_data) && size(Xt,1)==1)
    Xt_CI = []; % no confidence interval
else
    if ~isempty(prm.CI_data)
        Xt_CI = fct_squeeze_and_bring_dim_first(prm.CI_data, prm.Xdim);
    else
            prm_CI = struct;
            prm_CI.Nb = prm.CI_mth_Nb;
            prm_CI.alpha = prm.CI_mth_alpha;
        Xt_CI = fct_calc_CI(Xt, prm.CI_mth, prm_CI);
    end
end


% Def: 'Xt_p'
if ~prm.bsl_stat_plot || (isempty(prm.bsl_stat_data) && size(Xt,1)==1)
    Xt_p = []; % no p-values for statistical difference to baseline
else
    if ~isempty(prm.bsl_stat_data)
        Xt_p = fct_squeeze_and_bring_dim_first(prm.bsl_stat_data, prm.Xdim);
    else
        Xt_p = fct_calc_bsl_stat(Xt, prm.bsl, prm.bsl_stat_mth);
    end
end

%% Operation

hold on;
set(gca, 'tickdir', 'out');

% Plot: Baseline
if prm.bsl_plot
    y_bsl = prm.bsl + prm.offset;
    plot(t([1 end]), y_bsl*[1 1], prm.bsl_plot_linetype, 'color', prm.bsl_plot_col, 'linewidth', prm.bsl_plot_linewidth);
end

% Plot: Single trial Data
if prm.trials_plot && size(Xt,1)>1
    for x=1:size(Xt,1)
        plot(t, Xt(x,:) + prm.offset, 'color', [prm.trials_col prm.trials_alpha], 'linewidth', 1);
    end
end

% Plot: Data
switch prm.plot_mth
    case 'fill'
        if ~isempty(Xt_CI)
            fct_plot_fill_CI_t(t, Xt_CI + prm.offset, prm.plot_col, prm.plot_fillalpha)
        end
        plot(t, Xt_avg + prm.offset, prm.plot_linetype, 'color', [prm.plot_col prm.plot_linealpha], 'linewidth', prm.plot_linewidth);
        
    case 'err_bar'
        if ~isempty(Xt_CI)
            if numel(t)>1
                lrge_x = mean(t(2:end)-t(1:end-1));
            else
                lrge_x = 1;
            end
            for i=1:numel(t)
                CI = Xt_CI(:,i) + prm.offset;
                
                plot(t(i)*[1 1], CI, 'linewidth', prm.plot_linewidth, 'color', prm.plot_col);
                plot(t(i)+.05*lrge_x*[-1 1], CI(1)*[1 1], 'linewidth', prm.plot_linewidth, 'color', prm.plot_col);
                plot(t(i)+.05*lrge_x*[-1 1], CI(2)*[1 1], 'linewidth', prm.plot_linewidth, 'color', prm.plot_col);
            end
        end
        plot(t, Xt_avg + prm.offset, 'o', 'color', prm.plot_col, 'linewidth', prm.plot_linewidth, 'MarkerFaceColor', [1 1 1], 'MarkerSize', prm.plot_marksiz);
        if numel(t)>1
            xlim([min(t)-lrge_x/2 max(t)+lrge_x/2]);
        end
end

% Plot: Significance to baseline
if ~isempty(Xt_p)
        prm_sig = struct;
        prm_sig.col = prm.bsl_sig_col;
        prm_sig.lineW = prm.bsl_sig_linew;
        prm_sig.alpha = prm.bsl_sig_alpha;
    fct_plot_sig_Xt(Xt_p, t, prm.bsl + prm.offset + prm.bsl_sig_offset, prm_sig);
end



function mat = fct_squeeze_and_bring_dim_first(mat,dim)
%% Const
siz_dimX = size(mat,dim);

%% Operating
    
mat = permute(mat,[setdiff(1:ndims(mat),dim) min([dim ndims(mat)+1])]);
mat = squeeze(mat);
    
if siz_dimX==1
    mat = permute(mat,[ndims(mat)+1 1:ndims(mat)]);
else
    mat = permute(mat,[ndims(mat) 1:ndims(mat)-1]);
end



function val_X_ci = fct_calc_CI(val_X, CI_mth, prm_perc_bootsrp)
% function val_X_ci = fct_calc_CI(val_X, CI_method, prm_perc_bootsrp)
% 
% Computes the CI (confidence interval) 'val_X_CI' of the vector 'val_X'
%   CI_mth: 'ste'  'quartile'  '95p_med'  '95p_m'  '95p_tm'
%   prm_perc_bootsrp -> .Nb  .alpha  (used for '95p_*' as the percentile bootstrap parameters)
%
%
% if ~exist('CI_mth', 'var'),  CI_mth = '95p_m';   end
% if strncmp(CI_mth,'95p',3)
%     if ~exist('prm_perc_bootsrp','var'), prm_perc_bootsrp = struct; end
%     if ~isfield(prm_perc_bootsrp,'Nb'),      prm_perc_bootsrp.Nb = 100; end
%     if ~isfield(prm_perc_bootsrp,'alpha'),   prm_perc_bootsrp.alpha = .05; end
% end


% Dependence:
%   - pbci() - function from G. Rousselet

%%
if ~exist('CI_mth', 'var'),  CI_mth = '95p_m';   end
if strncmp(CI_mth,'95p',3)
    if ~exist('prm_perc_bootsrp','var'), prm_perc_bootsrp = struct; end
    if ~isfield(prm_perc_bootsrp,'Nb'),      prm_perc_bootsrp.Nb = 100; end
    if ~isfield(prm_perc_bootsrp,'alpha'),   prm_perc_bootsrp.alpha = .05; end
end



if any(size(val_X)==1)
    val_X = reshape(val_X,[],1);
end

switch CI_mth
    case 'std'         % CI=std (normalisation by n-1)
        val_X_ci = [1;1]*nanmean(val_X,1) + [-1;1]*nanstd(val_X,0,1); %*[-1 1]; %

    case 'ste'         % CI=ste (normalisation by n-1)
        val_X_ci = [1;1]*nanmean(val_X,1) + [-1;1]*nanstd(val_X,0,1) / sqrt(size(val_X,1)); %*[-1 1]; %
        
    case 'quartile'   % CI=quartiles
        val_X_ci = prctile(val_X, [25 75]);
        
    case '95p_med'     % CI= 95% de la mdiane
        for t=1:size(val_X,2)
            [~,~,val_X_ci(:,t),~] = pbci(val_X(:,t),prm_perc_bootsrp.Nb, prm_perc_bootsrp.alpha,'nanmedian');
        end
        
    case '95p_m'     % CI= 95% de la moyenne
        for t=1:size(val_X,2)
            [~,~,val_X_ci(:,t),~] = pbci(val_X(:,t),prm_perc_bootsrp.Nb, prm_perc_bootsrp.alpha,'nanmean');
        end
        
    case '95p_tm'     % CI= 95% de la moyenne (trimmed  20%)
        for t=1:size(val_X,2)
            [~,~,val_X_ci(:,t),~] = pbci(val_X(:,t),prm_perc_bootsrp.Nb, prm_perc_bootsrp.alpha,'nantm');
        end
        
    %case 'none'
    otherwise
        val_X_ci = nan(2,size(val_X,2));
    
        
end


function Xt_p = fct_calc_bsl_stat(Xt, bsl, bsl_stat_mth)
% function Xt_p = fct_calc_bsl_stat(Xt, bsl, bsl_stat_mth)
%
% Compute the difference to baseline (0, by default) on dimension 1

%% Consts


%% Input vars

if ~exist('bsl', 'var'), bsl=0;  end
if ~exist('bsl_stat_mth', 'var'), bsl_stat_mth = 'param';  end  % 'param'  'non-param'


%% Inits
Xt_p = nan(1,size(Xt,2));
        
%% Operation

switch bsl_stat_mth
    case {'param'}
        for t = 1:size(Xt,2)
            [~,Xt_p(t)] = ttest(Xt(:,t)-bsl);
        end
        
    case {'non-param'}
        for t = 1:size(Xt,2)
            Xt_p(t) = signrank(Xt(:,t)-bsl);
        end
        
end



function fct_plot_fill_CI_t(t, CI_t, col, facealpha)
% function fct_plot_fill_CI_t(t, CI_t, col, facealpha)
% 
% Simply fill a CI shape with some transparency - typically for the CI
% around a time serie average. Breaks the CI polygons in severals to account for nan values
%
% if nargin<1,  error('fct_plot_fill_CI_t(): two arg necessary.');   end
% if nargin<2,                         CI_t=[];   end
% if nargin<3 || isempty(col),         col=[0 94 176]/255;  end
% if nargin<4 || isempty(facealpha),   facealpha=.2;    end


%% Parameters (input vars)
if nargin<1,  error('fct_plot_fill_CI_t(): two arg necessary.');   end
if nargin<2,                         CI_t=[];   end
if nargin<3 || isempty(col),         col=[0 94 176]/255;  end
if nargin<4 || isempty(facealpha),   facealpha=.2;    end


%% Operation

    % Check 1
    if ~( ndims(t)==2 && any(size(t)==1) ), error('''%s'' is not a column or row vector, as required for the t values of a Xt plot.', inputname(1));   end
    if ~( ndims(CI_t)==2 ),                 error('''CI_t'' does not present 2 dimensions, as required for a CI plot.');   end

t = reshape(t, 1, []); % organize t in a row
CI_dim = find(size(CI_t)==2,1); % the first dim presenting a size of 2

    % Check 2
    if isempty(CI_dim), error('''CI_t'' does not present any dimension with size 2, as required for a CI plot.'); end

CI_t = permute(CI_t, [CI_dim setdiff(1:ndims(CI_t),CI_dim)]); % placing the dim presenting a size of 2 at first

    % Check 3
    if ~( size(t,2)==size(CI_t,2) ), error('''t'' and ''CI_t'' does not present the same number of elements in the t dimension required for a CI plot.');  end

if ~isempty(CI_t)
    non_nans = ~any(isnan(CI_t)) & ~isnan(t);
    first_non_nans = [find(non_nans(1)==1) find(diff(non_nans)==1)+1];
    last_non_nans = [find(diff(non_nans)==-1) find(non_nans(end)==1)*numel(non_nans)];
    for i=1:numel(first_non_nans)
        cur_t = t(first_non_nans(i):last_non_nans(i));
        cur_CI_t = CI_t(:,first_non_nans(i):last_non_nans(i));
        closed_t = [cur_t flip(cur_t) cur_t(1)];
        closed_CI_t = [cur_CI_t(1,:) flip(cur_CI_t(2,:)) cur_CI_t(1,1)];
        fill(closed_t, ...
             closed_CI_t, ...
             col, 'EdgeColor', 'none', 'FaceAlpha', facealpha);
    end
end



function fct_plot_sig_Xt(pval_t, times, y, param)
% function fct_plot_sig_Xt(pval_t, times, y, param)
% 
% if nargin<2,   error('Erreur: Au moins 2 arguments attendus.');  end
% if ~exist('y', 'var'),  y=0;  end
% if ~exist('param', 'var'),     param = struct;  end
% if ~isfield(param, 'col'),     param.col=[1 1 1]*0;  end
% if ~isfield(param, 'lineW'),   param.lineW = 3;   end
% if ~isfield(param, 'alpha'),   param.alpha = .01;   end
% 


%% Parameters (input vars)
if nargin<2,   error('Erreur: Au moins 2 arguments attendus.');  end
if ~exist('y', 'var'),  y=0;  end
if ~exist('param', 'var'),     param = struct;  end
if ~isfield(param, 'col'),     param.col=[1 1 1]*0;  end
if ~isfield(param, 'lineW'),   param.lineW = 3;   end
if ~isfield(param, 'alpha'),   param.alpha = .01;   end


%% Consts
dtp = times(2) - times(1);

%% Computation

sig_t = pval_t<param.alpha;

% [under construction: cluster-based approach of MCC]
if isfield(param, 'contig_filter')
    sig_t = fct_eeg_sig_contig_filter(reshape(sig_t,1,numel(sig_t)), param.contig_filter.chanlocs, param.contig_filter.prm);
    if isfield(param.contig_filter, 'filter_precog_t')
        sig_t(1:find(times<=param.contig_filter.filter_precog_t,1,'last'))=0;
    end
end

%% Operation
hold on;

for t = 1:numel(times)
    if sig_t(t)
        plot(times(t)+[-diff(times([max([t-1 1]) t])) ...
                        0 ...
                        diff(times([t min([t+1 numel(times)])]))] /2 ...
             , [0 0 0]+y, 'linewidth', param.lineW, 'color', param.col);
    end
end




